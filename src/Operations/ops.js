import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";



function Ops() {
  const [inputName, setInputName] = useState("");
  const [inputContact, setInputContact] = useState("");
  const [inputSalary, setInputSalary] = useState("");
  const [inputLocation, setInputLocation] = useState("");
  const [arr, setArr] = useState([]);
  const [toggleSubmit, setToggleSubmit] = useState(true);
  const [isEditData, setIsEditData] = useState("");

  function handleSubmit(e) {
    e.preventDefault();
    if (
      inputName &&
      inputContact &&
      inputSalary &&
      inputLocation &&
      !toggleSubmit
    ) {
      setArr(() => {
        return arr.map((obj) => {
          if (obj.id === isEditData) {
            return {
              ...obj,
              inputName,
              inputContact,
              inputSalary,
              inputLocation,
            };
          }
          return obj;
        });
      });
      setToggleSubmit(true);
      setInputContact("");
      setInputName("");
      setInputSalary("");
      setInputLocation("");
      setIsEditData(null);
    } else if (inputName && inputContact && inputSalary && inputLocation) {
      let uuid = uuidv4();
      let person = {
        id: uuid,
        inputName,
        inputContact,
        inputSalary,
        inputLocation,
      };
      setArr([...arr, person]);
    } else {
      alert("plz fill proper form");
    }
    setInputName("");
    setInputContact("");
    setInputSalary("");
    setInputLocation("");
  }

  function deleteData(id) {
    let temp = arr.filter((obj) => obj.id !== id);
    setArr(temp);
  }

  function editData(id) {
    let edit = arr.find((obj) => {
      return obj.id === id;
    });
    setToggleSubmit(false);
    setInputContact(edit.inputContact);
    setInputName(edit.inputName);
    setInputSalary(edit.inputSalary);
    setInputLocation(edit.inputLocation);
    setIsEditData(id);
  }

  return (
    <>
      <h1>Add Your Details</h1>
      <div className="addItem">
        <input
          type="text"
          Name=""
          value={inputName}
          placeholder="Enter Your Name..."
          onChange={(r) => setInputName(r.target.value)}
        />
        <input
          type="Number"
          Contact=""
          value={inputContact}
          placeholder="Enter Your Contact Number..."
          onChange={(r) => setInputContact(r.target.value)}
        />
        <input
          type="Number"
          Salary=""
          value={inputSalary}
          placeholder="Enter Your Salary..."
          onChange={(r) => setInputSalary(r.target.value)}
        />
        <input
          type="text"
          Location=""
          value={inputLocation}
          placeholder="Enter Your Location..."
          onChange={(r) => setInputLocation(r.target.value)}
        />
        {toggleSubmit ? (
          <button type="submit" Submit onClick={handleSubmit}>
            Submit
          </button>
        ) : (
          <button type="button" class="btn btn-primary" onClick={handleSubmit}>
            Update
          </button>
        )}
      </div>

      <div className="dataList">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Contact</th>
              <th>Salary</th>
              <th>Location</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {arr.map((obj) => {
              return (
                <tr key={obj.id}>
                  <td>{obj.inputName}</td>
                  <td>{obj.inputContact}</td>
                  <td>{obj.inputSalary}</td>
                  <td>{obj.inputLocation}</td>
                  <td>
                    <button
                      type="button"
                      class="btn btn-primary"
                      onClick={() => editData(obj.id)}
                    >
                      Edit
                    </button>
                  </td>
                  <td>
                    <button
                      type="button"
                      class="btn btn-danger"
                      onClick={() => deleteData(obj.id)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default Ops;
