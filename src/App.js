import "./App.css";
import Ops from "./Operations/ops";

function App() {
  return (
    <div className="App">
      <Ops />
    </div>
  );
}

export default App;
